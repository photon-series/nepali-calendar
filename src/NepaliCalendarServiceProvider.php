<?php
namespace PhotonSeries\NepaliCalendar;

use Illuminate\Support\ServiceProvider;

class NepaliCalendarServiceProvider extends ServiceProvider{

    public function register(){

    }

    public function boot(){
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadViewsFrom(__DIR__.'/views','calendar');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        $this->mergeConfigFrom(__DIR__.'/config/calendar.php','calendar');
        $this->commands([
            Console\Commands\CalendarCommand::class
        ]);
        $this->publishes([
            __DIR__.'/config/calendar.php' => config_path('calendar.php'),
            __DIR__.'/views' => resource_path('views/vendor'),
        ]);
        $this->publishes([
            __DIR__.'/database/migrations/' => database_path('migrations')
        ], 'calendar-migrations');
    }

}
