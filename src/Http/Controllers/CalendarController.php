<?php

namespace PhotonSeries\NepaliCalendar\Http\Controllers;

use App\Http\Controllers\Controller;
use PhotonSeries\NepaliCalendar\Services\CalendarService;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    protected $calendarServices;
    public function __construct(CalendarService $calendarServices)
    {
        $this->calendarServices = $calendarServices;
    }
    public function index(){
        return config('contact.year_from');
        // return view('nepalicalendar::calendar.index');
    }
    public function list(){
        try {
            return $this->calendarServices->list();
        }catch(\Exception $e){
            return $e->getMessage();
        }
    }
}
