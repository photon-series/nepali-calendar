<?php

namespace PhotonSeries\NepaliCalendar\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Nilambar\NepaliDate\NepaliDate;

class Calendar extends Model
{
    use HasFactory;

    protected $guarded=[];
    protected $appends = ['np_from','np_to'];
    public function getNpFromAttribute()
    {
        $obj = new NepaliDate();
        $exp_from= explode('-',$this->attributes['from']);
        $date = $obj->convertAdToBs($exp_from[0], $exp_from[1], $exp_from[2]);
        $month = $date['month'];
        $month = $month>9?$month:"0$month";
        $day = $date['day'];
        $day = $day>9?$day:"0$day";
        return "$date[year]-$month-$day";
    }
    public function getNpToAttribute()
    {
        $obj = new NepaliDate();
        $exp_to= explode('-',$this->attributes['to']);
        $date = $obj->convertAdToBs($exp_to[0], $exp_to[1], $exp_to[2]);
        $month = $date['month'];
        $month = $month>9?$month:"0$month";
        $day = $date['day'];
        $day = $day>9?$day:"0$day";
        return "$date[year]-$month-$day";
    }
}
