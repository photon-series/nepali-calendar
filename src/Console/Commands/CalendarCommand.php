<?php

namespace PhotonSeries\NepaliCalendar\Console\Commands;

use Illuminate\Console\Command;
use DOMDocument;
use Illuminate\Http\Request;
use Goutte\Client;
use Illuminate\Support\Facades\DB;
use Symfony\Component\DomCrawler\Crawler;
use Nilambar\NepaliDate\NepaliDate;

class CalendarCommand extends Command
{
    public $data=[];
    public $holidays=[];
    public $i=0;
    public $year;
    public $month;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calendar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize nepali calender';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->calendar();
        $this->info('Calendar synchronized successfully.');
    }
    public function calendar()
    {
        try {
            $year_from = (int) config('calendar.year_from');
            $year_to = (int) config('calendar.year_to');
            if($year_from>0 && $year_to>=$year_from){
                $this->scrapper($year_from,$year_to);
            }else{
                $this->info('************************************* INVALID DATE RANGE *************************************');
            }
        } catch (\Exception $e) {
            $msg = strtoupper($e->getMessage());
            $this->info("************************************* $msg *************************************");
        }
    }

    public function scrapper($year_from,$year_to){
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('calendars')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        $client = new Client();
        for ($year = $year_from; $year <= $year_to; $year++) {
            $this->year = $year;
            for ($m = 1; $m <= 12; $m++) {
                $this->month = $m;
                $this->holidays=[];
                $this->info("Scraping information for $year-$m ...");
                $crawler = $client->request('GET', "https://www.hamropatro.com/calendar/$this->year/$this->month");
                $crawler->filter('.dates>li>span.nep')->each(function ($node, $key){
                    if(($key+1)%7==0 && $this->saturdayCond($node->text())){
                        array_push($this->holidays,$node->text());
                    }
                });
                $crawler->filter('.dates>li.disable')->each(function (Crawler $crawler) {
                    foreach ($crawler as $node) {
                        $node->parentNode->removeChild($node);
                    }
                });
                $crawler->filter('.dates>li.holiday>span.nep')->each(function ($node, $key){
                    array_push($this->holidays,$node->text());
                });
                $crawler->filter('.dates>li>span.nep')->each(function ($node, $key){
                    $this->data[$key]['is_holiday']=in_array($node->text(),$this->holidays);
                    $month=(int)$this->month;
                    $month = $month>9?$month:"0$month";
                    $day=(int)$this->replaceUnicode($node->text());
                    $day = $day>9?$day:"0$day";
                    $this->data[$key]['from']="$this->year-$month-$day";
                });
                $crawler->filter('.dates>li>span.event')->each(function ($node, $key){
                    $this->data[$key]['title']=$node->text();
                });
                $this->i=0;
                $crawler->filter('.dates>li>span.tithi')->each(function ($node, $key){
                    if($node->text()){
                        $this->data[$this->i]['sub_title']=$node->text();
                        $this->i = $this->i + 1;
                    }
                });
                $this->syncCalendar();
                $this->info("Calendar synchronized successfully for $year-$m.");
            }
        }
    }

    public function syncCalendar(){
        $obj = new NepaliDate();
        foreach ($this->data as $data){
            $exp_from = explode('-',$data['from']);
            $from = $obj->convertBsToAd($exp_from[0],$exp_from[1],$exp_from[2]);
            $from_date = "$from[year]-$from[month]-$from[day]";
            $ifExist = DB::table('calendars')
                ->where(['title'=>$data['title']])
                ->whereDate('from',$from_date)
                ->first();
            if(!$ifExist){
                DB::table('calendars')->insert([
                    'title'=>$data['title'],
                    'sub_title'=>$data['sub_title'],
                    'from'=>$from_date,
                    'to'=>$from_date,
                    'is_holiday'=>$data['is_holiday'],
                ]);
            }
        }
    }

    public function saturdayCond($node){
        if(count($this->holidays)>0){
            $en_node = $this->replaceUnicode($node);
            $last_node = $this->replaceUnicode($this->holidays[count($this->holidays)-1]);
            return (int)$en_node > (int)$last_node;
        }
        return true;
    }

    public function replaceUnicode($node){
        $searchVal = array('०', '१', '२', '३', '४', '५', '६', '७', '८', '९');
        $replaceVal = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        return str_replace($searchVal, $replaceVal, $node);
    }
}
