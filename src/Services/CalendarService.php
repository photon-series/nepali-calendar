<?php

namespace PhotonSeries\NepaliCalendar\Services;
use PhotonSeries\NepaliCalendar\Models\Calendar;
class CalendarService
{
    public function list()
    {
        return Calendar::select('id','title','sub_title','from','to','description','is_holiday')->get();
    }

}
