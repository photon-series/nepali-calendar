<?php
Route::group(["namespace"=>"PhotonSeries\NepaliCalendar\Http\Controllers"],function(){
    Route::get('/calendar/list',"CalendarController@list");
    Route::get('/calendar',"CalendarController@index");
});
