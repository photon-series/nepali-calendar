## About Nepali Calendar

Nepali Calendar helps you to keep track of important dates, events, tithis etc in your own system.

## Installation

```bash
composer require photon-series/nepali-calendar
```

## Usage

Publish everything it finds, this includes configs, views, migrations, and more.
```bash
php artisan vendor:publish
```
<small><i>Provider: Icts\Nepalicalander\NepaliCalanderServiceProvider .................................................</i></small> <br>

To sync database with hamro patro detes, events, holidays and more.
```bash
php artisan calendar
```
OR
```php
use Illuminate\Support\Facades\Artisan;
Artisan::call("calendar");
```

### Initialize Date Converter

```php
use Nilambar\NepaliDate\NepaliDate;

$obj = new NepaliDate();

// Convert BS to AD.
$date = $obj->convertBsToAd('2077', '1', '1');

// Convert AD to BS.
$date = $obj->convertAdToBs('2020', '1', '1');

// Get Nepali date details by BS date.
$date = $obj->getDetails('2077', '1', '1', 'bs');

// Get Nepali date details by AD date.
$date = $obj->getDetails('2020', '1', '1', 'ad');
```

## Contributing

Thank you for considering! The contribution guide can be found in the [Package Development](https://laravel.com/docs/9.x/packages).

## Code of Conduct

In order to ensure that welcoming to all, please review and abide.

## Security Vulnerabilities

If you discover a security vulnerability within package, please send an e-mail to ALOK CHAUDHARY via [alokcdhy@gmail.com](mailto:alokcdhy@gmail.com). All security vulnerabilities will be promptly addressed.

